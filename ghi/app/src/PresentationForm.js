import React, { useEffect, useState } from 'react';

function PresentationhtmlForm() {
    const [conferences, setConferences] = useState([]);
    const [presenterName, setPresenterName] = useState("");
    const [presenterEmail, setPresenterEmail] = useState("");
    const [companyName, setCompanyName] = useState("");
    const [title, setTitle] = useState("");
    const [synopsis, setSynopsis] = useState("");
    const [conference, setConference] = useState("");

    const handlePNameChange = (event) => {
        const value = event.target.value;
        setPresenterName(value);
    }

    const handlePEmailChange = (event) => {
        const value = event.target.value;
        setPresenterEmail(value);
    }

    const handleCompanyChange = (event) => {
        const value = event.target.value;
        setCompanyName(value);
    }

    const handleTitleChange = (event) => {
        const value = event.target.value;
        setTitle(value);
    }

    const handleSynopsisChange = (event) => {
        const value = event.target.value;
        setSynopsis(value);
    }

    const handleConferenceChange = (event) => {
        const value = event.target.value;
        setConference(value);
    }


    const fetchData = async () => {
        const url = "http://localhost:8000/api/conferences/";

        try {
            const response = await fetch(url);
            if (!(response.ok)) {
                console.error("Invalid response from api/conferences!!");
            } else {
                const data = await response.json();
                setConferences(data.conferences);
            }
        } catch(e) {
            console.error("Error!! ", e);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};

        data.presenter_name = presenterName;
        data.company_name = companyName;
        data.presenter_email = presenterEmail;
        data.synopsis = synopsis;
        data.title = title;

        const url = `http://localhost:8000/api/conferences/${Number(conference)}/presentations/`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            const newPresentation = await response.json();
            console.log(newPresentation);

            setPresenterName("");
            setPresenterEmail("");
            setCompanyName("");
            setTitle("");
            setSynopsis("");
            setConference("");
        }
    }

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit={handleSubmit} id="create-presentation-form">
              <div className="form-floating mb-3">
                <input onChange={handlePNameChange} value={presenterName} placeholder="Presenter name" required type="text" id="presenter_name" name="presenter_name" className="form-control" />
                <label htmlFor="presenter_name">Presenter name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handlePEmailChange} value={presenterEmail} placeholder="Presenter email" required type="email" id="presenter_email" name="presenter_email" className="form-control" />
                <label htmlFor="presenter_email">Presenter Email</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleCompanyChange} value={companyName} placeholder="Company name" type="text" id="company_name" name="company_name" className="form-control" />
                <label htmlFor="company_name">Company name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleTitleChange} value={title} placeholder="Title" required type="text" id="title" name="title" className="form-control" />
                <label htmlFor="title">Title</label>
              </div>
              <div className="mb-3">
                <label htmlFor="synopsis">Synopsis</label>
                <textarea onChange={handleSynopsisChange} value={synopsis} className="form-control" required type="text" name="synopsis" id="synopsis" rows="3"></textarea>
              </div>
              <div className="mb-3">
                <select onChange={handleConferenceChange} value={conference} required id="conference" name="conference" className="form-select">
                    <option key="" value="">Choose a conference</option>
                    {conferences.map(conference => {
                        return (
                            <option key={conference.href} value={conference.pk}>
                                {conference.name}
                            </option>
                        );
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}

export default PresentationhtmlForm;
